<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use \Illuminate\Http\Response as Res;

use App\UserStats;
use App\User;
use DB;
use Carbon\Carbon;
use App\Http\Requests\URRequest;

class UsersController extends Controller
{
    //

    public function register(Request $request)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();
            
            $user = User::firstOrNew(['email' => $input['email']]);

            $user_info = [
                'full_name' => $input['full_name'],
                'password' => bcrypt($input['password']),
                'email' =>($input['email']),
                'uuid' => str_random(10),
                'timezone' => "5:00",
                'status' => 1,
                'last_seen' => Carbon::now(),
            ];
            
            $data = $user->fill($user_info)->save();

            if ($data) {
                $user_stats = UserStats::create([
                    'user_id_fk' => $user['user_id'],
                    'dob' => null,
                    'height' => '0',
                    'age' => '0',
                    'start_weight' => '0',
                    'first_day_last_period' => null,
                    'cycle_length' => '0',
                    'conceive_date' => null,
                    'estimated_due_date' => null,
                ]);
            }
            DB::commit();

            return $this->respond([
                'status' => 'success',
                'status_code' => 200,
                'message' => 'User register successfully!',
            ]);
        } catch (Exception $e) {
            DB::rollBack();
            
            return $this->respond([
                'status' => 'failure',
                'status_code' => 400,
                'message' => 'User login un-successful!',
            ]);
            
        }
    }
}
