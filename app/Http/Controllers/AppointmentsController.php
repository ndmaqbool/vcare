<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Response;
use Illuminate\Http\Response as Res;
use Exception;
use Log;
use App\Appointments;
use DB;

class AppointmentsController extends Controller
{
    //
    public function bookAppointment(Request $request) {

        try {
            DB::beginTransaction();

            $input = $request->all();
                       
            $appointment = Appointments::create([
                'appointment_time' => $input['appointment_datetime'],
                'doctor_id' => $input['doctor_id'],
                'hospital_id' => $input['hospital_id'],
                'lab_id' => 0,
                'patient_id' => $input['user_id'],
                'location' => $input['location'],
                'pregnancy_week' => $input['week_no'],
                'appointment_type' => $input['appointment_type'],
                'status' => 0
            ]);
                
            DB::commit();
            
            return $this->respond([
                'status' => 'success',
                'status_code' => 200,
                'message' => 'Book appointment successfully!',
            ]);

        } catch (Exception $e) {
            DB::rollBack();
            Log::error(
                'Book GP Appointment method exception (bookGpAppointment()):' . PHP_EOL .
                'File: ' . $e->getFile() . PHP_EOL .
                'Line: ' . $e->getLine() . PHP_EOL .
                $e->getMessage() . PHP_EOL . PHP_EOL . $e->getTraceAsString()
            );
            return $this->respond([
                'status' => 'failure',
                'status_code' => 400,
                'message' => 'Unable to book apppintment',
            ]);
        }
    }

    public function deleteAppointment(Request $request) {
        try {
            $input = $request->all();
            $query = Appointments::where('appointment_id','=', $input['appointment_id'])
                                ->where('patient_id', $input['user_id'])
                                ->delete();
            
            return $this->respond([
                'status' => 'success',
                'status_code' => 200,
                'message' => 'Appointment deleted successfully',
            ]);
            
        } catch(Exception $e) {
            Log::error(
                'delete appointment, method exception (deleteAppointment()):' . PHP_EOL .
                'File: ' . $e->getFile() . PHP_EOL .
                'Line: ' . $e->getLine() . PHP_EOL .
                $e->getMessage() . PHP_EOL . PHP_EOL . $e->getTraceAsString()
            );

            return $this->respond([
                'status' => 'failure',
                'status_code' => 400,
                'message' => 'Unable to deleted appointment',
            ]);
        }
    }
}
