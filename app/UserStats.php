<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserStats extends Model
{
    protected $table = 'user_stats';

    /**
     * This attributes defines primary key of the table
     *
     * @var string
     */
    protected $primaryKey = 'user_stat_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id_fk',
        'dob',
        'height',
        'start_weight',
        'first_day_last_period',
        'cycle_length',
        'conceive_date',
        'age',
        'due_date_calc_flag',
        'dob',
        'estimated_due_date'
    ];
}
