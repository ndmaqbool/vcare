<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\AppointmentsController;
use Illuminate\Http\Request;
use \Illuminate\Http\Response;


class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUserRegistation()
    {
       // $this->assertTrue(true);
       
       $request = Request::create('/register', 'POST',[
            "full_name" => "shobi",
            "email" => "shobi@test.com",
            "password" => "test@123"
        ]);
       $controller = new UsersController();
       $response = $controller->register($request);

       $this->assertEquals(200, $response->getStatusCode());
       //when i have data for user to register i add user to db
    }

    public function testAppointmentBooking()
    {
       $request = Request::create('/book-appointment', 'POST',[
            'appointment_datetime' => "2018-12-19 09:10:00",
            'doctor_id' => 1,
            'hospital_id' => 3,
            'lab_id' => 0,
            'user_id' => 2,
            'location' => "Menakaskus",
            'week_no' => 6,
            'appointment_type' => "hospital",
            'status' => 0
        ]);
       $aController = new AppointmentsController();
       $response = $aController->bookAppointment($request);

       $this->assertEquals(200, $response->getStatusCode());
       //when i have data for user to register i add user to db
    }
    
    public function testDeleteAppointment()
    {
       $request = Request::create('/delete-appointment', 'DELETE',[
            'appointment_id' => "2",
            'user_id' => 1,
        ]);
       $aController = new AppointmentsController();
       $response = $aController->deleteAppointment($request);

       $this->assertEquals(200, $response->getStatusCode());
       //when i have data for user to register i add user to db
    }
}
